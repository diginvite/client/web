import React from "react";
import NumberFormat from "react-currency-format";

const Pricing = (props) => {
  return (
    <section id="pricing" className="call-action-area">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="section-title text-center pb-20">
              <h3 className="title">Pricing Plan</h3>
              <p className="text">
                Berikut adalah pilihan paket yang bisa kamu pilih
              </p>
            </div>
          </div>
        </div>
        <div className="row justify-content-center">
          {props.data.map((data, i) => {
            return (
              <div className="col-lg-3 col-md-7 col-sm-9" key={i}>
                <div
                  className="pricing-style-one mt-40 wow fadeIn"
                  data-wow-duration="1.5s"
                  data-wow-delay="0.2s"
                >
                  <div className="pricing-icon text-center">
                    <img src="assets/images/wman.svg" alt="" />
                  </div>
                  <div className="pricing-header text-center">
                    <h5 className="sub-title">{data.name}</h5>
                    {data.price < data.priceDiscount ? (
                      <NumberFormat
                        value={data.priceDiscount}
                        displayType={"text"}
                        thousandSeparator={true}
                        prefix=""
                        style={{ textDecoration: "line-through", color: "red" }}
                      />
                    ) : (
                      <p>-</p>
                    )}
                    <p className="month">
                      <span className="price">
                        <NumberFormat
                          value={data.price}
                          displayType={"text"}
                          thousandSeparator={true}
                          prefix=""
                        />
                      </span>
                    </p>
                  </div>
                  <div className="pricing-list">
                    <ul>
                      {props.features.map((feature, i2) => {
                        let textDecoration = "line-through";
                        let color = "red";
                        let icon = (
                          <i
                            style={{ color: "red" }}
                            className="lni-cross-circle"
                          ></i>
                        );
                        if (
                          data.features.some(
                            (selectedFeature) =>
                              selectedFeature.id === feature.id
                          )
                        ) {
                          textDecoration = "none";
                          color = "null";
                          icon = <i className="lni-check-mark-circle"></i>;
                        }
                        return (
                          <li key={i2}>
                            {icon}
                            <span
                              style={{
                                color: color,
                                textDecoration: textDecoration,
                              }}
                            >
                              {feature.name}
                            </span>
                            {/* &nbsp; */}
                            {/* {feature.detail.quantity > 0 ? (
                                <>({feature.detail.quantity})</>
                              ) : null} */}
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                  {data.name === "Premium" ? (
                    <div className="pricing-btn rounded-buttons text-center">
                      <p className="main-btn rounded-three">Terlaris</p>
                    </div>
                  ) : null}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Pricing;
