import React from "react";

const Template = (props) => {
  return (
    <section id="portfolio" className="pricing-area">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="section-title text-center pb-20">
              <h3 className="title">Template Undangan</h3>
              <p className="text">
                Berikut adalah template yang bisa kamu pilih untuk undangan
                digital kamu.
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="portfolio-menu pt-30 text-center">
              <ul>
                <li
                  className={props.tab === "all" ? "active" : ""}
                  onClick={() => props.onFilter("all")}
                >
                  ALL TEMPLATE
                </li>
                <li
                  className={props.tab === 0 ? "active" : ""}
                  onClick={() => props.onFilter(false)}
                >
                  STANDAR
                </li>
                <li
                  className={props.tab === 1 ? "active" : ""}
                  onClick={() => props.onFilter(true)}
                >
                  PREMIUM
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="row">
          {props.data.map((data, i) => {
            return (
              <div
                className={`col-lg-3 col-sm-6 ${data.isPremium} planning-3`}
                key={i}
              >
                <div
                  className="single-portfolio mt-30 wow fadeInUp"
                  data-wow-duration="1.5s"
                  data-wow-delay="0.2s"
                >
                  <div className="portfolio-image">
                    {data.image !== null ? (
                      <img src={data.images[0].url} alt="templateImage" />
                    ) : (
                      <img
                        src="https://preview.uideck.com/items/smart-demo/assets/smart/assets/images/portfolio-3.png"
                        alt=""
                      />
                    )}
                    <div className="portfolio-overlay d-flex align-items-center justify-content-center">
                      <div className="portfolio-content">
                        {/* <div className="portfolio-icon">
                                            <a className="image-popup" href="https://preview.uideck.com/items/smart-demo/assets/smart/assets/images/portfolio-3.png"><i className="lni-zoom-in"></i></a>
                                        </div> */}
                        <div className="portfolio-icon">
                          <a
                            href={`http://${data.domain}.diginvite.com?invite=john-doe`}
                            target="blank"
                          >
                            <i className="lni-link"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="portfolio-text">
                    <h4 className="portfolio-title">
                      <a
                        href={`http://${data.domain}.diginvite.com?invite=john-doe`}
                        target="blank"
                      >
                        {data.isPremium ? (
                          <i
                            className="lni-star-filled"
                            style={{ color: "#FFDF00" }}
                          ></i>
                        ) : null}
                        &nbsp;
                        {data.name}
                      </a>
                    </h4>
                    <p className="text">{data.description}</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Template;
