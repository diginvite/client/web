import React from "react";
import logo from "../logo.png";

const Header = () => {
  return (
    <div className="navbar-area">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <nav className="navbar navbar-expand-lg">
              <a className="navbar-brand" href="#here">
                <img src={logo} alt="Logo" />
                {/* <h3 style={{color: "white"}}>DIGINVITE</h3> */}
              </a>

              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarEight"
                aria-controls="navbarEight"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="toggler-icon"></span>
                <span className="toggler-icon"></span>
                <span className="toggler-icon"></span>
              </button>

              <div
                className="collapse navbar-collapse sub-menu-bar"
                id="navbarEight"
              >
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item active">
                    <a
                      style={{ color: "#6c6c6c" }}
                      className="page-scroll"
                      href="#home"
                    >
                      HOME
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      style={{ color: "#6c6c6c" }}
                      className="page-scroll"
                      href="#about"
                    >
                      FEATURE
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      style={{ color: "#6c6c6c" }}
                      className="page-scroll"
                      href="#portfolio"
                    >
                      TEMPLATE
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      style={{ color: "#6c6c6c" }}
                      className="page-scroll"
                      href="#pricing"
                    >
                      PRICING
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      style={{ color: "#6c6c6c" }}
                      className="page-scroll"
                      href="#order"
                    >
                      ORDER
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      style={{ color: "#6c6c6c" }}
                      className="page-scroll"
                      href="#client"
                    >
                      PARTNER
                    </a>
                  </li>
                  {/* <li className="nav-item">
                                        <a className="page-scroll" href="#testimonial">TESTIMONIAL</a>
                                    </li> */}
                  <li className="nav-item">
                    <a
                      style={{ color: "#6c6c6c" }}
                      className="page-scroll"
                      href="#contact"
                    >
                      CONTACT
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      style={{ color: "#6c6c6c" }}
                      className="page-scroll"
                      href="https://crm.diginvite.com"
                      target="blank"
                    >
                      CRM
                    </a>
                  </li>
                </ul>
              </div>

              <div className="navbar-btn d-none mt-15 d-lg-inline-block">
                <a
                  style={{ color: "#6c6c6c" }}
                  className="menu-bar"
                  href="#side-menu-right"
                >
                  <i className="lni-menu"></i>
                </a>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
