import React from "react";

const Partner = (props) => {
  return (
    <section id="client" class="client-logo-area">
      <div className="container">
        <div className="row">
          {props.data.map((partner, i) => {
            return (
              <div className="col-lg-2">
                <div className="single-client text-center">
                  <img src={partner.image.url} alt="Logo" />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Partner;
