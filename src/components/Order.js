import React, { useState } from "react";
// import NumberFormat from 'react-currency-format';

const Order = (props) => {
  const [order, setOrder] = useState({
    domain: "",
    name: "",
    email: "",
    phone: "",
    description: "",
    package: {},
    template: {},
  });

  const onChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setOrder({ ...order, [name]: value });
  };

  const onPackageChange = (e) => {
    setOrder({ ...order, package: JSON.parse(e.target.value) });
  };

  const onTemplateChange = (e) => {
    setOrder({ ...order, template: JSON.parse(e.target.value) });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    props.onSubmit(order);
    // setOrder({
    //   domain: "",
    //   name: "",
    //   email: "",
    //   phone: "",
    //   description: "",
    //   package: {},
    //   template: {},
    // });
  };

  return (
    <section id="order" className="pricing-area">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="section-title text-center pb-20">
              <h3 className="title">Order</h3>
              <p className="text">Order undangan digital kamu tanpa ribet</p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <div
              className="contact-two mt-50 wow fadeIn"
              data-wow-duration="1.5s"
              data-wow-delay="0.2s"
            >
              <h4 className="contact-title">Langkah-langkah Order</h4>
              <p className="text">
                Berikut langkah-langkah order undangan digital DIGINVITE
              </p>
              <ul className="contact-info">
                <li>
                  <b>1. Isi Form</b>
                  <br />
                  Kamu bisa order dengan cara mengisi form order atau chat via
                  whatsapp.
                </li>
                <li>
                  <b>2. Input Data</b>
                  <br />
                  Input data undangan kamu pada form yang telah disediakan.
                </li>
                <li>
                  <b>3. Proses Pengerjaan</b>
                  <br />
                  Proses pengerjaan undangan oleh team DIGINVITE sesuai data
                  yang dimasukan, termasuk dengan revisi-revisi jika ada
                  perubahan data dll.
                </li>
                <li>
                  <b>4. Proses Pembayaran</b>
                  <br />
                  Silahkan transfer biaya dan konfirmasi pembayaran di link yang
                  dikirim oleh DIGINVITE. Pembarayan dilakukan setelah proses
                  pembuatan undangan selesai.
                </li>
                <li>
                  <b>5. Selesai</b>
                  <br />
                  Undangan kamu siap dikirim dan diakses oleh keluaraga, teman
                  dan kerabat.
                </li>
              </ul>
            </div>
          </div>
          <div className="col-lg-6">
            <div
              className="contact-form form-style-one mt-35 wow fadeIn"
              data-wow-duration="1.5s"
              data-wow-delay="0.5s"
            >
              <form id="contact-form" onSubmit={onSubmit}>
                <div className="form-input mt-15">
                  <label>Domain</label>
                  <div className="input-items default">
                    <input
                      type="text"
                      placeholder="Domain"
                      onChange={onChange}
                      value={order.domain}
                      name="domain"
                    />
                    <i className="lni-domain"></i>
                  </div>
                </div>
                <div className="form-input mt-15">
                  <label>Name</label>
                  <div className="input-items default">
                    <input
                      type="text"
                      placeholder="Name"
                      onChange={onChange}
                      value={order.name}
                      name="name"
                    />
                    <i className="lni-user"></i>
                  </div>
                </div>
                <div className="form-input mt-15">
                  <label>Email</label>
                  <div className="input-items default">
                    <input
                      type="email"
                      placeholder="Email"
                      onChange={onChange}
                      value={order.email}
                      name="email"
                    />
                    <i className="lni-envelope"></i>
                  </div>
                </div>
                <div className="form-input mt-15">
                  <label>Phone</label>
                  <div className="input-items default">
                    <input
                      type="text"
                      placeholder="Phone"
                      onChange={onChange}
                      value={order.phone}
                      name="phone"
                    />
                    <i className="lni-phone"></i>
                  </div>
                </div>
                <div className="form-input mt-15">
                  <label>Package</label>
                  <div className="input-items default">
                    <select
                      className="input-text"
                      name="package"
                      value={props.package}
                      onChange={onPackageChange}
                    >
                      <option value="">-- Select package --</option>
                      {props.packages.map((data, i) => {
                        return (
                          <option key={i} value={JSON.stringify(data)}>
                            {data.name} - Rp. {data.price}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                </div>
                <div className="form-input mt-15">
                  <label>Template</label>
                  <div className="input-items default">
                    <select
                      className="input-text"
                      name="template"
                      value={props.template}
                      onChange={onTemplateChange}
                    >
                      <option value="">-- Select template --</option>
                      {props.templates.map((data, i) => {
                        return (
                          <option key={i} value={JSON.stringify(data)}>
                            {data.name}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                </div>
                <div className="form-input mt-15">
                  <label>Note (Optional)</label>
                  <div className="input-items default">
                    <textarea
                      placeholder="Note"
                      onChange={onChange}
                      value={order.description}
                      name="description"
                    ></textarea>
                    <i className="lni-pencil-alt"></i>
                  </div>
                </div>
                <p className="form-message"></p>
                <div className="form-input rounded-buttons mt-20">
                  <button type="submit" className="main-btn rounded-three">
                    Order
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Order;
