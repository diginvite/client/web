import React from 'react';
import logo from '../logo.png'

const Footer = () => {
  return(
    <footer id="footer" className="footer-area">
        {/* <div className="footer-widget">
            <div className="container">
                <div className="row">
                    <div className="col-lg-3 col-sm-6">
                        <div className="footer-link">
                            <h6 className="footer-title">Company</h6>
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Profile</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="footer-link">
                            <h6 className="footer-title">Solutions</h6>
                            <ul>
                                <li><a href="#">Facilities Services</a></li>
                                <li><a href="#">Workplace Staffing</a></li>
                                <li><a href="#">Project Management</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="footer-link">
                            <h6 className="footer-title">Product & Services</h6>
                            <ul>
                                <li><a href="#">Products</a></li>
                                <li><a href="#">Business</a></li>
                                <li><a href="#">Developer</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="footer-link">
                            <h6 className="footer-title">Help & Suuport</h6>
                            <ul>
                                <li><a href="#">Support Center</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> */}
        
        <div className="footer-copyright">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-5">
                        <div className="copyright text-center text-lg-left mt-10">
                            <p className="text">
                              Made with <i className="lni-heart-filled" style={{color: "red"}}></i> by <a style={{color: "#38f9d7"}} rel="nofollow" href="https://ajatdarojat45.id" target="blank"> lazyCode </a>
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-2">
                        <div className="footer-logo text-center mt-10">
                            <a href="#home"><img src={logo} alt="Logo" style={{maxHeight: "25px"}}/></a>
                            {/* <a href="index.html">DIGINVITE</a> */}
                        </div>
                    </div>
                    <div className="col-lg-5">
                        <ul className="social text-center text-lg-right mt-10">
                          <li><a href="https://facebook.com/diginvitte" target="blank"><i className="lni-facebook-filled"></i></a></li>
                          <li><a href="https://twitter.com/diginvite_" target="blank"><i className="lni-twitter-original"></i></a></li>
                          <li><a href="https://instagram.com/diginvite_" target="blank"><i className="lni-instagram-original"></i></a></li>
                          {/* <li><a href="#"><i className="lni-linkedin-original"></i></a></li> */}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  )
}

export default Footer;

