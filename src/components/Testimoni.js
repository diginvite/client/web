import React from 'react';

const Testimoni = () => {
  return(
    <section id="testimonial" className="testimonial-area">
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-lg-6">
                    <div className="section-title text-center pb-20">
                        <h3 className="title">Testimonial</h3>
                        <p className="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                    </div>
                </div>
            </div>
            
            <div className="row">
                <div className="col-lg-12">
                    <div className="row testimonial-active">
                        <div className="col-lg-4">
                            <div className="single-testimonial mt-30 mb-30 text-center">
                                <div className="testimonial-image">
                                    <img src="assets/images/author-3.jpg" alt="Author"/>
                                </div>
                                <div className="testimonial-content">
                                    <p className="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 className="author-name">Isabela Moreira</h6>
                                    <span className="sub-title">CEO, GrayGrids</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="single-testimonial mt-30 mb-30 text-center">
                                <div className="testimonial-image">
                                    <img src="assets/images/author-3.jpg" alt="Author"/>
                                </div>
                                <div className="testimonial-content">
                                    <p className="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 className="author-name">Isabela Moreira</h6>
                                    <span className="sub-title">CEO, GrayGrids</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="single-testimonial mt-30 mb-30 text-center">
                                <div className="testimonial-image">
                                    <img src="assets/images/author-3.jpg" alt="Author"/>
                                </div>
                                <div className="testimonial-content">
                                    <p className="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 className="author-name">Isabela Moreira</h6>
                                    <span className="sub-title">CEO, GrayGrids</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="single-testimonial mt-30 mb-30 text-center">
                                <div className="testimonial-image">
                                    <img src="assets/images/author-3.jpg" alt="Author"/>
                                </div>
                                <div className="testimonial-content">
                                    <p className="text">Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed! Stop wasting time and money designing and managing a website that doesn’t get results. Happiness guaranteed!</p>
                                    <h6 className="author-name">Isabela Moreira</h6>
                                    <span className="sub-title">CEO, GrayGrids</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Testimoni;

