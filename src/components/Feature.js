import React from 'react';

const Feature = (props) => {
  return(
    <section id="about" className="call-action-area">
      <div className="container">
          <div className="row justify-content-center">
                <div className="col-lg-6">
                    <div className="section-title text-center pb-20">
                        <h3 className="title">Fitur Undangan</h3>
                        <p className="text">Apa saja yang kamu dapatkan dari udangan DIGINVITE?</p>
                    </div>
                </div>
            </div>
          
          <div className="row">
            {
              props.data.map((data, i) => {
                return(
                  <div className="col-lg-6" key={i}>
                      <div className="single-about d-sm-flex mt-30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="1.2s">
                          <div className="about-icon">
                              {/* <img src="https://preview.uideck.com/items/smart-demo/assets/smart/assets/images/icon-3.png" alt="Icon"/> */}
                              <i style={{color: "#38f9d7"}} className="lni-check-mark-circle"></i>
                          </div>
                          <div className="about-content media-body">
                              <h4 className="about-title">{data.name}</h4>
                              <p className="text" style={{textAlign: "justify"}}>{data.description}</p>
                          </div>
                      </div>
                  </div>
                )
              })
            }
          </div>
      </div>
    </section>
  )
}

export default Feature;

