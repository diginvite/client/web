import React from 'react';

const Contact = (props) => {
  return(
    <section id="contact" className="contact-area">
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-lg-6">
                    <div className="section-title text-center pb-20">
                        <h3 className="title">Kontak</h3>
                        <p className="text"></p>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-6">
                    <div className="contact-two mt-50 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.2s">
                        <h4 className="contact-title">Kontak kami</h4>
                        <p className="text"> Kamu bisa menguhungi kami melalu email atau telepon.</p>
                        <ul className="contact-info">
                            <li><i className="lni-money-location"></i> Rumpin, Kabupaten Bogor, Indonesia</li>
                            <li><i className="lni-phone-handset"></i> +62 821-1225-7695</li>
                            <li><i className="lni-envelope"></i> info@diginvite.com</li>
                        </ul>
                    </div>
                </div>
                <div className="col-lg-6">
                    <div className="contact-form form-style-one mt-35 wow fadeIn" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <form  id="contact-form" onSubmit={props.onSubmit}>
                            <div className="form-input mt-15">
                                <label>Name</label>
                                <div className="input-items default">
                                    <input type="text" placeholder="Name" name="name" disabled/>
                                    <i className="lni-user"></i>
                                </div>
                            </div>
                            <div className="form-input mt-15">
                                <label>Email</label>
                                <div className="input-items default">
                                    <input type="email" placeholder="Email" name="email" disabled/>
                                    <i className="lni-envelope"></i>
                                </div>
                            </div>
                            <div className="form-input mt-15">
                                <label>Massage</label>
                                <div className="input-items default">
                                    <textarea placeholder="Massage" name="massage" disabled></textarea>
                                    <i className="lni-pencil-alt"></i>
                                </div>
                            </div>
                            <p className="form-message"></p>
                            <div className="form-input rounded-buttons mt-20">
                                <button type="submit" className="main-btn rounded-three">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Contact;

