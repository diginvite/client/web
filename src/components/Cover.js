import React from 'react';

const Cover = () => {
  return(
    <div id="home" className="slider-area">
          <div className="bd-example">
              <div id="carouselOne" className="carousel slide" data-ride="carousel">
                  <ol className="carousel-indicators">
                      <li data-target="#carouselOne" data-slide-to="0" className="active"></li>
                      <li data-target="#carouselOne" data-slide-to="1"></li>
                      <li data-target="#carouselOne" data-slide-to="2"></li>
                      <li data-target="#carouselOne" data-slide-to="3"></li>
                      <li data-target="#carouselOne" data-slide-to="4"></li>
                      <li data-target="#carouselOne" data-slide-to="5"></li>
                  </ol>

                  <div className="carousel-inner">
                      <div className="carousel-item bg_cover active" 
                        // style="background-image: url(assets/images/slider-1.jpg)"
                      >
                          <div className="carousel-caption">
                              <div className="container">
                                  <div className="row justify-content-center">
                                      <div className="col-xl-6 col-lg-7 col-sm-10">
                                          <h2 className="carousel-title">UNDANGAN PERNIKAHAN DIGITAL</h2>
                                          <h5 style={{color: "white"}} >BUAT UNDANGAN PERNIKAHAN DIGITAL KAMU</h5>
                                          {/* <ul className="carousel-btn rounded-buttons">
                                              <li><a className="main-btn rounded-three" href="#">GET STARTED</a></li>
                                              <li><a className="main-btn rounded-one" href="#">DOWNLOAD</a></li>
                                          </ul> */}
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div className="carousel-item bg_cover" 
                        // style="background-image: url(assets/images/slider-2.jpg)"
                      >
                          <div className="carousel-caption">
                              <div className="container">
                                  <div className="row justify-content-center">
                                      <div className="col-xl-6 col-lg-7 col-sm-10">
                                          <h2 className="carousel-title">LEBIH BANYAK FITUR</h2>
                                          <h5 style={{color: "white"}} >DENGAN FITUR-FITUR KEREN</h5>
                                          {/* <ul className="carousel-btn rounded-buttons">
                                              <li><a className="main-btn rounded-three" href="#">GET STARTED</a></li>
                                              <li><a className="main-btn rounded-one" href="#">DOWNLOAD</a></li>
                                          </ul> */}
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div className="carousel-item bg_cover" 
                      // style="background-image: url(assets/images/slider-3.jpg)"
                      >
                          <div className="carousel-caption">
                              <div className="container">
                                  <div className="row justify-content-center">
                                      <div className="col-xl-6 col-lg-7 col-sm-10">
                                          <h2 className="carousel-title">LEBIH BANYAK TEMPLATE</h2>
                                          <h5 style={{color: "white"}} >
                                            DENGAN LEBIH BANYAK PILIHAN TEMPLATE MENARIK
                                          </h5>
                                          {/* <ul className="carousel-btn rounded-buttons">
                                              <li><a className="main-btn rounded-three" href="#">GET STARTED</a></li>
                                              <li><a className="main-btn rounded-one" href="#">DOWNLOAD</a></li>
                                          </ul> */}
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div className="carousel-item bg_cover" 
                      // style="background-image: url(assets/images/slider-3.jpg)"
                      >
                          <div className="carousel-caption">
                              <div className="container">
                                  <div className="row justify-content-center">
                                      <div className="col-xl-6 col-lg-7 col-sm-10">
                                          <h2 className="carousel-title">HARGA TEJANGKAU</h2>
                                          <h5 style={{color: "white"}} >BUAT UNDANGAN PERNIKHAKAN KAMU DANGAN BUDGET MINIMUM</h5>
                                          {/* <ul className="carousel-btn rounded-buttons">
                                              <li><a className="main-btn rounded-three" href="#">GET STARTED</a></li>
                                              <li><a className="main-btn rounded-one" href="#">DOWNLOAD</a></li>
                                          </ul> */}
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div className="carousel-item bg_cover" 
                      // style="background-image: url(assets/images/slider-3.jpg)"
                      >
                          <div className="carousel-caption">
                              <div className="container">
                                  <div className="row justify-content-center">
                                      <div className="col-xl-6 col-lg-7 col-sm-10">
                                          <h2 className="carousel-title">CARA ORDER MUDAH</h2>
                                          <h5 style={{color: "white"}} >ORDER PEMBUATAN UNDANGAN PERNIKAHAN DIGITAL KAMU TANPA RIBET</h5>
                                          {/* <ul className="carousel-btn rounded-buttons">
                                              <li><a className="main-btn rounded-three" href="#">GET STARTED</a></li>
                                              <li><a className="main-btn rounded-one" href="#">DOWNLOAD</a></li>
                                          </ul> */}
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div className="carousel-item bg_cover" 
                      // style="background-image: url(assets/images/slider-3.jpg)"
                      >
                          <div className="carousel-caption">
                              <div className="container">
                                  <div className="row justify-content-center">
                                      <div className="col-xl-6 col-lg-7 col-sm-10">
                                          <h2 className="carousel-title">BAYAR BELAKANGAN</h2>
                                          <h5 style={{color: "white"}} >PEMBAYARAN DILAKUKAN SETELAH UNDANGAN DIGITAL KAMU SELESAI ATAU JADI</h5>
                                          {/* <ul className="carousel-btn rounded-buttons">
                                              <li><a className="main-btn rounded-three" href="#">GET STARTED</a></li>
                                              <li><a className="main-btn rounded-one" href="#">DOWNLOAD</a></li>
                                          </ul> */}
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <a className="carousel-control-prev" href="#carouselOne" role="button" data-slide="prev">
                      <i className="lni-arrow-left-circle"></i>
                  </a>

                  <a className="carousel-control-next" href="#carouselOne" role="button" data-slide="next">
                      <i className="lni-arrow-right-circle"></i>
                  </a>
              </div>
          </div>
      </div>
  )
}

export default Cover;