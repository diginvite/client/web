import React from "react";
import logo from "../logo.png";

const Sidebar = () => {
  return (
    <>
      <div className="sidebar-right">
        <div className="sidebar-close">
          <a className="close" href="#close">
            <i className="lni-close"></i>
          </a>
        </div>
        <div className="sidebar-content">
          <div className="sidebar-logo text-center">
            <a href="#here">
              <img src={logo} alt="Logo" />
            </a>
          </div>
          <div className="sidebar-menu">
            <ul>
              <li>
                <a href="#home" style={{ color: "#6c6c6c" }}>
                  HOME
                </a>
              </li>
              <li>
                <a href="#about" style={{ color: "#6c6c6c" }}>
                  FEATURE
                </a>
              </li>
              <li>
                <a href="#pricing" style={{ color: "#6c6c6c" }}>
                  PRICING
                </a>
              </li>
              <li>
                <a href="#order" style={{ color: "#6c6c6c" }}>
                  ORDER
                </a>
              </li>
              <li>
                <a href="#client" style={{ color: "#6c6c6c" }}>
                  PARTNER
                </a>
              </li>
              <li>
                <a href="#contact" style={{ color: "#6c6c6c" }}>
                  CONTACT
                </a>
              </li>
              <li>
                <a
                  href="https://crm.diginvite.com"
                  target="blank"
                  style={{ color: "#6c6c6c" }}
                >
                  CRM
                </a>
              </li>
            </ul>
          </div>
          <div className="sidebar-social d-flex align-items-center justify-content-center">
            <span>FOLLOW US</span>
            <ul>
              <li>
                <a href="https://instagram.com/diginvite_" target="blank">
                  <i className="lni-instagram-original"></i>
                </a>
              </li>
              <li>
                <a href="https://twitter.com/diginvite_" target="blank">
                  <i className="lni-twitter-original"></i>
                </a>
              </li>
              <li>
                <a href="https://facebook.com/diginvitte" target="blank">
                  <i className="lni-facebook-filled"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="overlay-right"></div>
    </>
  );
};

export default Sidebar;
