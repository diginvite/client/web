import React, { Component } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import "../App.css";
import Preload from "../components/Preloader";
import Header from "../components/Header";
import Cover from "../components/Cover";
import Sidebar from "../components/Sidebar";
import Feature from "../components/Feature";
import Template from "../components/Template";
import Pricing from "../components/Pricing";
// import Subscribe from '../components/Subscribe';
// import Testimoni from '../components/Testimoni';
import Partner from "../components/Partner";
import Contact from "../components/Contact";
import Footer from "../components/Footer";
import Order from "../components/Order";

import globalUrl from "../actions/config";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      packages: [],
      partners: [],
      features: [],
      templates: [],
      templatesMaster: [],
      templateTab: "all",
      loading: true,
      domain: "",
      name: "",
      email: "",
      phone: "",
      description: "",
      package: "",
      template: "",
    };
  }

  fetchData = async () => {
    const { data: features } = await axios.get(`${globalUrl}/findFeatures`);
    const { data: templates } = await axios.get(`${globalUrl}/findTemplates`);
    const { data: packages } = await axios.get(`${globalUrl}/findPackages`);
    const { data: partners } = await axios.get(`${globalUrl}/findPartners`);
    this.setState({
      features,
      templates,
      packages,
      partners,
      templatesMaster: templates,
      loading: false,
    });
  };

  componentDidMount = () => {
    this.fetchData();
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = (order) => {
    if (order.domain === "") {
      toast.error("Maaf domain tidak boleh kosong", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      if (order.name === "") {
        toast.error("Maaf nama tidak boleh kosong", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } else {
        if (order.email === "") {
          toast.error("Maaf email tidak boleh kosong", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          if (order.phone === "") {
            toast.error("Maaf phone tidak boleh kosong", {
              position: toast.POSITION.TOP_RIGHT,
            });
          } else {
            if (order.package === "") {
              toast.error("Silahkan pilih paket", {
                position: toast.POSITION.TOP_RIGHT,
              });
            } else {
              if (order.template === "") {
                toast.error("Silahkan pilih template", {
                  position: toast.POSITION.TOP_RIGHT,
                });
              } else {
                const data = {
                  domain: order.domain,
                  name: order.name,
                  email: order.email,
                  phone: order.phone,
                  package: order.package,
                  template: order.template,
                  description: order.description,
                };

                axios
                  .post(`${globalUrl}/order`, data)
                  .then((response) => {
                    toast.success(
                      "Order anda berhsil dibuat, silahkan menunggu di hubungi pihak kami.",
                      {
                        position: toast.POSITION.TOP_RIGHT,
                      }
                    );
                  })
                  .catch((error) => {
                    toast.error(error.response.data.message, {
                      position: toast.POSITION.TOP_RIGHT,
                    });
                  });
              }
            }
          }
        }
      }
    }
  };

  onSubmit2 = (e) => {
    e.preventDefault();
    toast.error(
      "Maaf guest book sedang dalam perbaikan, silahkan chat via whatsapp.",
      {
        position: toast.POSITION.TOP_RIGHT,
      }
    );
  };

  onTemplateFilter = (param) => {
    var templates = this.state.templatesMaster;
    if (param === "all") {
      this.setState({ templates: templates, templateTab: "all" });
    } else {
      var templatesFiltered = templates.filter(function (template) {
        return template.isPremium === param;
      });
      this.setState({ templates: templatesFiltered, templateTab: param });
    }
  };

  render() {
    return (
      <>
        <Preload />
        <section className="header-area">
          <Header />
          <Cover />
        </section>
        <Sidebar />
        <Feature data={this.state.features} />
        <Template
          data={this.state.templates}
          tab={this.state.templateTab}
          onFilter={(param) => this.onTemplateFilter(param)}
        />
        <Pricing data={this.state.packages} features={this.state.features} />
        <Order
          packages={this.state.packages}
          templates={this.state.templates}
          onSubmit={(order) => this.onSubmit(order)}
        />
        {/* <Subscribe/> */}
        {/* <Testimoni/> */}
        <Partner data={this.state.partners} />
        <Contact onSubmit={(e) => this.onSubmit2(e)} />
        <Footer />
        <a
          href="#home"
          className="back-to-top"
          style={{ display: "block", bottom: "90px" }}
        >
          <i className="lni-chevron-up"></i>
        </a>
        <a
          href="https://api.whatsapp.com/send?phone=6282112257695&text=Halo%20DIGINVITE"
          className="Whatsapp-btn"
          style={{ display: "block" }}
          target="blank"
        >
          <i className="lni-whatsapp"></i>
        </a>
        <ToastContainer />
      </>
    );
  }
}

export default Home;
